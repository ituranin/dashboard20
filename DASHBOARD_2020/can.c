/*
 * can.c
 *
 * Created: 13.02.2020 11:59:58
 *  Author: Igor
 */ 

#include "can.h"

static struct timer_task syslogTimer;
static struct timer_task commandTimer;
static struct timer_task targetValueTimer;

struct can_message syslogMsg;
struct can_message commandMsg;
struct can_message targetValueMsg;

struct can_filter  filter;

uint8_t syslogMsgData[8];
uint8_t commandMsgData[8];
uint8_t targetValueMsgData[8];

static void syslogTimerTask(const struct timer_task *const timer_task)
{
	sendSyslogMessage();
}

static void commandTimerTask(const struct timer_task *const timer_task)
{
	sendCommandMessage();
}

static void targetValueTimerTask(const struct timer_task *const timer_task)
{
	sendTargetValueMessage();
}

void canReceiveCallback(struct can_async_descriptor *const descr)
{
	struct can_message msg;
	uint8_t data[8];
	msg.data = data;
	can_async_read(descr, &msg);
	
	// mostly copy from Dashboard18 project
	switch (msg.id)
	{
		// ECU
		case 0x200://settings.dashMsgBaseID + 0x0:
		{
			if(dashData.batSysType == liFePo && dashData.ascuIsConnected == false)
			{
				dashData.batVoltAct = (msg.data[0]<<8) + msg.data[1];		//overwrite only, if no ascu data is received
			}
			dashData.oilTemp = (msg.data[2]<<8) + msg.data[3];
			dashData.waterTemp1 = (msg.data[4]<<8) + msg.data[5];
			dashData.oilPressure = (msg.data[6]<<8) + msg.data[7];
			//TODO water temp2 CAN field
			dashData.waterTemp2 = dashData.waterTemp1;
			break;
		}
		case 0x201://settings.dashMsgBaseID + 0x1:
		{
			dashData.gear = (msg.data[0]<<8) + msg.data[1];
			dashData.rpm = (msg.data[2]<<8) + msg.data[3];
			dashData.speed = (msg.data[4]<<8) + msg.data[5];
			dashData.waterTemp2 = (msg.data[6]<<8) + msg.data[7];
			dashData.gear -= 2;
			dashData.speed = (3*dashData.speed)>>3;		//calc to 10* kilometers per hour
			break;
		}
		case 0x202://settings.dashMsgBaseID + 0x2:
		{
			dashData.prevLaptime = (msg.data[0]<<8) + msg.data[1];
			dashData.laptime = (msg.data[2]<<8) + msg.data[3];
			dashData.dLaptime = (msg.data[4]<<8) + msg.data[5];
			dashData.limpMode = (msg.data[6]<<8) + msg.data[7];
			break;
		}
		case 0x203://settings.dashMsgBaseID + 0x3:
		{
			dashData.brakePressureFront = (msg.data[0]<<8) + msg.data[1];
			dashData.brakePressureRear = (msg.data[2]<<8) + msg.data[3];
			break;
		}
		// ASSC
		case 0x120:	//syslog assc
		{
			//get global error bit
			if(0b00010000 & msg.data[7])
			{
				dashData.ebsFailure = true;
			}
			else
			{
				dashData.ebsFailure = false;
			}
			// TODO CPU, IMU, LIDAR, CAMERA availability bits
			break;
		}
		case 0x140:	//assc bat voltages, ebs voltage, 5V sensor1
		{
			dashData.ascuIsConnected = true;
			
			if(dashData.batActType == liFePo)
			{
				dashData.batVoltAct = (msg.data[0]<<8) + msg.data[1];
			}

			if(dashData.batSysType == liFePo)
			{
				dashData.batVoltSys = (msg.data[4]<<8) + msg.data[5];
			}
			break;
		}
		// BMS
		case 0x411:
		{
			dashData.batActType = liIon;
			dashData.batVoltAct = (msg.data[0]<<8) + msg.data[1];

		}
		case 0x421:
		{
			dashData.batSysType = liIon;
			dashData.batVoltSys = (msg.data[0]<<8) + msg.data[1];

		}
		// AMI
		case 0x129:
		{
			dashData.amiState = msg.data[2];
		}
		default:;
	}
	
	return;
}

void initDataFieldsWithZeros()
{
	for (uint8_t i = 0; i < 8; i++)
	{
		syslogMsgData[i] = 0;
		commandMsgData[i] = 0;
		targetValueMsgData[i] = 0;
	}
}

void initCanTimers()
{
	syslogTimer.interval = 100;
	syslogTimer.mode = TIMER_TASK_REPEAT;
	syslogTimer.cb = syslogTimerTask;
	timer_add_task(&TIMER_0, &syslogTimer);
	
	commandTimer.interval = 1;
	commandTimer.mode = TIMER_TASK_REPEAT;
	commandTimer.cb = commandTimerTask;
	timer_add_task(&TIMER_0, &commandTimer);
	
	targetValueTimer.interval = 10;
	targetValueTimer.mode = TIMER_TASK_REPEAT;
	targetValueTimer.cb = targetValueTimerTask;
	timer_add_task(&TIMER_0, &targetValueTimer);
}

void initCAN()
{
	initDataFieldsWithZeros();
	
	syslogMsg.id = 0x126;
	syslogMsg.len = 8;
	syslogMsg.data = syslogMsgData;
	
	commandMsg.id = 0x100;
	commandMsg.len = 8;
	commandMsg.data = commandMsgData;
	
	targetValueMsg.id = 0x146;
	targetValueMsg.len = 8;
	targetValueMsg.data = targetValueMsgData;
	
	filter.id   = 0x0;
	filter.mask = 0;
	can_async_set_filter(&CAN_0, 0, CAN_FMT_STDID, &filter);
	
	can_async_register_callback(&CAN_0, CAN_ASYNC_RX_CB, (FUNC_PTR)canReceiveCallback);
	can_async_enable(&CAN_0);
	initCanTimers();
}

void sendSyslogMessage()
{
	syslogMsg.data[0] = dashData.state;
	can_async_write(&CAN_0, &syslogMsg);
}

void sendCommandMessage()
{
	if (!dashData.sendCommandMessage)
	{
		return;
	}
	
	commandMsg.data[0] = dashData.shiftRequestState;
	can_async_write(&CAN_0, &commandMsg);
	dashData.sendCommandMessage = false;
}

void sendTargetValueMessage()
{
	can_async_write(&CAN_0, &targetValueMsg);
}