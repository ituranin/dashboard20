#include "global.h"
#include "screens.h"
#include "can.h"

static struct timer_task cycleTimer;
static struct timer_task uiTimer;
static struct timer_task ledTimer;

inline void resetShiftRequest()
{
	if (dashData.shiftRequestState != CLEARSHIFT_REQUEST && !dashData.sendCommandMessage)
	{
		clearShiftRequest();
	}
}

// TODO keep this timer for automatic shift request reset
static void cycleTimerTask(const struct timer_task *const timer_task)
{
	resetShiftRequest();
	adc_async_start_conversion(&ADC_0);
	//dashData.rpm += 10;
	dashData.speed = 50;
	
	if(dashData.rpm > 12500)
	{
		//dashData.rpm = 7000;
	}
	
	if(dashData.speed > 1100)
	{
		dashData.speed = 0; // (3*speed)>>3 TODO
	}
}

static void uiTimerTask(const struct timer_task *const timer_task)
{
	requestRedrawScreen();
}

static void ledTimerTask(const struct timer_task *const timer_task)
{
	setGearDisplay();
	// TODO add back
	setRpmLeds();
	setActorBatteryLed();
	setSystemBatteryLed();
	setEbsIndicatorLed();
	setOilPressureLow();
}

void initCycleTimer()
{
	cycleTimer.interval = 10;
	cycleTimer.cb       = cycleTimerTask;
	cycleTimer.mode     = TIMER_TASK_REPEAT;
	timer_add_task(&TIMER_0, &cycleTimer);
	
	uiTimer.interval = 33;
	uiTimer.cb       = uiTimerTask;
	uiTimer.mode     = TIMER_TASK_REPEAT;
	timer_add_task(&TIMER_0, &uiTimer);
	
	ledTimer.interval = 10;
	ledTimer.cb       = ledTimerTask;
	ledTimer.mode     = TIMER_TASK_REPEAT;
	timer_add_task(&TIMER_0, &ledTimer);
}

static void shiftUpPressed(void)
{
	bool level = gpio_get_pin_level(SHIFT_PADDLE_UP);
	
	if (screenState.currentScreen->btnShiftUpHandler != 0)
	{
		screenState.currentScreen->btnShiftUpHandler(level);
	}
}

static void shiftDownPressed(void)
{
	bool level = gpio_get_pin_level(SHIFT_PADDLE_DOWN);
	
	if (screenState.currentScreen->btnShiftDownHandler != 0)
	{
		screenState.currentScreen->btnShiftDownHandler(level);
	}
}

static void switchButtonTopLeftPressed(void)
{
	bool level = gpio_get_pin_level(BTN_TOP_LEFT);
	
	if (screenState.currentScreen->btnTopLeftHandler != 0)
	{
		screenState.currentScreen->btnTopLeftHandler(level);
	}
}

static void switchButtonTopRightPressed(void)
{
	bool level = gpio_get_pin_level(BTN_TOP_RIGHT);
	
	if (screenState.currentScreen->btnTopRightHandler != 0)
	{
		screenState.currentScreen->btnTopRightHandler(level);
	}
}

static void switchButtonMidLeftPressed(void)
{
	bool level = gpio_get_pin_level(BTN_MID_LEFT);
	
	if (screenState.currentScreen->btnMidLeftHandler != 0)
	{
		screenState.currentScreen->btnMidLeftHandler(level);
	}
}

static void switchButtonMidRightPressed(void)
{
	bool level = gpio_get_pin_level(BTN_MID_RIGHT);
	
	if (screenState.currentScreen->btnMidRightHandler != 0)
	{
		screenState.currentScreen->btnMidRightHandler(level);
	}
}

static void switchButtonBottomLeftPressed(void)
{
	bool level = gpio_get_pin_level(BTN_BOTTOM_LEFT);
	
	if (screenState.currentScreen->btnBottomLeftHandler != 0)
	{
		screenState.currentScreen->btnBottomLeftHandler(level);
	}
}

static void switchButtonBottomRightPressed(void)
{
	bool level = gpio_get_pin_level(BTN_BOTTOM_RIGHT);
	
	if (screenState.currentScreen->btnBottomRightHandler != 0)
	{
		screenState.currentScreen->btnBottomRightHandler(level);
	}
}

void initButtons()
{
	ext_irq_register(SHIFT_PADDLE_UP, shiftUpPressed);
	ext_irq_register(SHIFT_PADDLE_DOWN, shiftDownPressed);
	ext_irq_register(BTN_TOP_LEFT, switchButtonTopLeftPressed);
	ext_irq_register(BTN_TOP_RIGHT, switchButtonTopRightPressed);
	ext_irq_register(BTN_MID_LEFT, switchButtonMidLeftPressed);
	ext_irq_register(BTN_MID_RIGHT, switchButtonMidRightPressed);
	ext_irq_register(BTN_BOTTOM_LEFT, switchButtonBottomLeftPressed);
	ext_irq_register(BTN_BOTTOM_RIGHT, switchButtonBottomRightPressed);
}

uint16_t to16bit(uint8_t low, uint8_t high)
{
	return low | (high << 8);
}


uint8_t buffer[2];
static void adcCallback(const struct adc_async_descriptor *const descr, const uint8_t channel)
{
	adc_async_read_channel(descr, channel, buffer, 2);
	dashData.rpm = to16bit(buffer[0], buffer[1]) * 3;
}

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	
	adc_async_register_callback(&ADC_0, 0, ADC_ASYNC_CONVERT_CB, adcCallback);
	adc_async_enable_channel(&ADC_0, 0);
	adc_async_start_conversion(&ADC_0);
	
	initCAN();

	spi_m_async_get_io_descriptor(&SPI_0, &oled_io);
	spi_m_async_enable(&SPI_0);
	oledl128_initialize(OLED_RESET, DC, DC, OLEDL128, true);
	
	initScreens();
	
	initCycleTimer();
	timer_start(&TIMER_0);
	
	initButtons();

	/* Replace with your application code */
	while (1) {
		updateScreen();
	}
}

