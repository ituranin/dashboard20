/*
 * can.h
 *
 * Created: 13.02.2020 11:57:29
 *  Author: Igor
 */ 


#ifndef CAN_H_
#define CAN_H_

#include "global.h"

void initCAN();
void sendSyslogMessage();
void sendCommandMessage();
void sendTargetValueMessage();

#endif /* CAN_H_ */