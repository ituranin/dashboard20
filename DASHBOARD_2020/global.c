/*
 * global.c
 *
 * Created: 10.02.2020 13:30:22
 *  Author: Igor
 */ 

#include "global.h"

DashData dashData =
{
	.autoShiftEnabled = false,
	.batVoltAct = 12000,
	.batVoltSys = 12000,
	.dLaptime = 0,
	.ebsFailure = false,
	.engineIsRunning = false,
	.gear = 1, // TODO set 0
	.laptime = 0,
	.limpMode = 0,
	.oilPressure = 0,
	.oilTemp = 200,
	.prevLaptime = 0,
	.beaconTriggered = 0,
	.rpm = 7000,
	.speed = 0,
	.wheelSpeedFL = 0,
	.wheelSpeedFR = 0,
	.wheelSpeedRL = 0,
	.wheelSpeedRR = 0,
	.waterTemp1 = 180,
	.waterTemp2 = 180,
	.brakePressureFront = 2150,
	.brakePressureRear = 50,
	.blockFastestLaptime = 2,
	.ascuIsConnected = false,
	.shiftLightFlashCounter = 0,
	.shiftRequestState = 0,
	.sendCommandMessage = false,
	.shutDownCircuitState = 0,
	.hydraulicsPumpState = 0,
	.state = 0,
	.batSysType = 0,
	.batActType = 0,
	.amiState = 0,
	.stateCPU = 0,
	.stateIMU = 0,
	.stateCamera = 0,
	.stateLidar = 0
};

void downshiftRequest()
{
	if (checkDownshiftConditions())
	{
		// TODO remove
		dashData.gear--;
		dashData.shiftRequestState = DOWNSHIFT_REQUEST;
		dashData.sendCommandMessage = true;
	}
}

void upshiftRequest()
{
	if (checkUpshiftConditions())
	{
		// TODO remove
		dashData.gear++;
		dashData.shiftRequestState = UPSHIFT_REQUEST;
		dashData.sendCommandMessage = true;
	}
}

void clearShiftRequest()
{
	dashData.shiftRequestState = CLEARSHIFT_REQUEST;
	dashData.sendCommandMessage = true;
}

bool checkUpshiftConditions()
{
	// TODO clutch pressure check if engine is running?
	if (dashData.gear == 0 || dashData.gear < 4)
	{
		return true;
	}
	
	return false;
}

bool checkDownshiftConditions()
{
	if (dashData.gear > 0)
	{
		if (dashData.gear == 1 && dashData.speed > MAX_DOWNSHIFT_SPEED_FIRST_GEAR_RAW)
		{
			return false;
		}
		
		return true;
	}
	
	return false;
}

void setGearDisplay() {
	switch (dashData.gear)
	{
		case 0:
		{
			gpio_set_pin_level(GEAR_A, false);
			gpio_set_pin_level(GEAR_B, false);
			gpio_set_pin_level(GEAR_C, false);
			break;
		}
		case 1:
		{
			gpio_set_pin_level(GEAR_A, true);
			gpio_set_pin_level(GEAR_B, false);
			gpio_set_pin_level(GEAR_C, false);
			break;
		}
		case 2:
		{
			gpio_set_pin_level(GEAR_A, false);
			gpio_set_pin_level(GEAR_B, true);
			gpio_set_pin_level(GEAR_C, false);
			break;
		}
		case 3:
		{
			gpio_set_pin_level(GEAR_A, true);
			gpio_set_pin_level(GEAR_B, true);
			gpio_set_pin_level(GEAR_C, false);
			break;
		}
		case 4:
		{
			gpio_set_pin_level(GEAR_A, false);
			gpio_set_pin_level(GEAR_B, false);
			gpio_set_pin_level(GEAR_C, true);
			break;
		}
		case 5:
		{
			gpio_set_pin_level(GEAR_A, true);
			gpio_set_pin_level(GEAR_B, false);
			gpio_set_pin_level(GEAR_C, true);
			break;
		}
		case 6:
		{
			gpio_set_pin_level(GEAR_A, false);
			gpio_set_pin_level(GEAR_B, true);
			gpio_set_pin_level(GEAR_C, true);
			break;
		}
		case 7:
		{
			gpio_set_pin_level(GEAR_A, true);
			gpio_set_pin_level(GEAR_B, true);
			gpio_set_pin_level(GEAR_C, true);
			break;
		}
		default: break;
	}
}

void setRpmLeds()
{
	if (dashData.rpm <= SHIFT_INDICATOR_RPM_OFF)
	{
		gpio_set_pin_level(SHIFTLIGHT_IN0, false);
		gpio_set_pin_level(SHIFTLIGHT_IN1, false);
		gpio_set_pin_level(SHIFTLIGHT_IN2, false);
		dashData.shiftLightFlashCounter = 0;
		return;
	}
	
	if (dashData.rpm > SHIFT_INDICATOR_RPM_3)
	{
		dashData.shiftLightFlashCounter++;
		
		gpio_set_pin_level(SHIFTLIGHT_IN0, true);
		gpio_set_pin_level(SHIFTLIGHT_IN1, true);
		
		if (dashData.shiftLightFlashCounter <= LED_FLASH_COUNTER_INFLECTION)
		{
			gpio_set_pin_level(SHIFTLIGHT_IN2, true);
		}
		else
		{
			gpio_set_pin_level(SHIFTLIGHT_IN2, false);
		}
		
		if (dashData.shiftLightFlashCounter > LED_FLASH_COUNTER_RESET)
		{
			dashData.shiftLightFlashCounter = 0;
		}
		
		return;
	}
	
	if (dashData.rpm > SHIFT_INDICATOR_RPM_2)
	{
		gpio_set_pin_level(SHIFTLIGHT_IN0, true);
		gpio_set_pin_level(SHIFTLIGHT_IN1, true);
		gpio_set_pin_level(SHIFTLIGHT_IN2, false);
		dashData.shiftLightFlashCounter = 0;
		return;
	}
	
	if (dashData.rpm > SHIFT_INDICATOR_RPM_1)
	{
		gpio_set_pin_level(SHIFTLIGHT_IN0, false);
		gpio_set_pin_level(SHIFTLIGHT_IN1, true);
		gpio_set_pin_level(SHIFTLIGHT_IN2, false);
		dashData.shiftLightFlashCounter = 0;
		return;
	}
	
	if (dashData.rpm > SHIFT_INDICATOR_RPM_OFF)
	{
		gpio_set_pin_level(SHIFTLIGHT_IN0, true);
		gpio_set_pin_level(SHIFTLIGHT_IN1, false);
		gpio_set_pin_level(SHIFTLIGHT_IN2, false);
		dashData.shiftLightFlashCounter = 0;
		return;
	}
}

void setActorBatteryLed()
{
	if (dashData.batVoltAct < ACTOR_MIN_VOLTAGE_RAW)
	{
		gpio_set_pin_level(STATUS_ORANGE_LEFT, true);
		return;
	}
	
	gpio_set_pin_level(STATUS_ORANGE_LEFT, false);
}

void setSystemBatteryLed()
{
	if (dashData.batVoltSys < SYSTEM_MIN_VOLTAGE_RAW)
	{
		gpio_set_pin_level(STATUS_ORANGE_RIGHT, true);
		return;
	}
	
	gpio_set_pin_level(STATUS_ORANGE_RIGHT, false);
}

void setEbsIndicatorLed()
{
	if (dashData.ebsFailure)
	{
		gpio_set_pin_level(STATUS_RED_RIGHT, true);
		return;
	}
	
	gpio_set_pin_level(STATUS_RED_RIGHT, false);
}

void setOilPressureLow()
{
	if (dashData.oilPressure < OIL_PRESSURE_MIN_RAW && dashData.rpm > 0)
	{
		gpio_set_pin_level(STATUS_RED_LEFT, true);
		return;
	}
	
	gpio_set_pin_level(STATUS_RED_LEFT, false);
}

void setRpmLedsDebug()
{
	if (dashData.rpm <= SHIFT_INDICATOR_RPM_OFF)
	{
		gpio_set_pin_level(STATUS_ORANGE_LEFT, false);
		gpio_set_pin_level(STATUS_RED_LEFT, false);
		gpio_set_pin_level(STATUS_RED_RIGHT, false);
		gpio_set_pin_level(STATUS_ORANGE_RIGHT, false);
		dashData.shiftLightFlashCounter = 0;
		return;
	}
	
	if (dashData.rpm > SHIFT_INDICATOR_RPM_3)
	{
		dashData.shiftLightFlashCounter++;
		
		gpio_set_pin_level(STATUS_ORANGE_LEFT, true);
		gpio_set_pin_level(STATUS_RED_LEFT, true);
		gpio_set_pin_level(STATUS_RED_RIGHT, true);
		
		if (dashData.shiftLightFlashCounter <= LED_FLASH_COUNTER_INFLECTION)
		{
			gpio_set_pin_level(STATUS_ORANGE_RIGHT, true);
		}
		else
		{
			gpio_set_pin_level(STATUS_ORANGE_RIGHT, false);
		}
		
		if (dashData.shiftLightFlashCounter > LED_FLASH_COUNTER_RESET)
		{
			dashData.shiftLightFlashCounter = 0;
		}
		
		return;
	}
	
	if (dashData.rpm > SHIFT_INDICATOR_RPM_2)
	{
		gpio_set_pin_level(STATUS_ORANGE_LEFT, true);
		gpio_set_pin_level(STATUS_RED_LEFT, true);
		gpio_set_pin_level(STATUS_RED_RIGHT, true);
		gpio_set_pin_level(STATUS_ORANGE_RIGHT, false);
		dashData.shiftLightFlashCounter = 0;
		return;
	}
	
	if (dashData.rpm > SHIFT_INDICATOR_RPM_1)
	{
		gpio_set_pin_level(STATUS_ORANGE_LEFT, true);
		gpio_set_pin_level(STATUS_RED_LEFT, true);
		gpio_set_pin_level(STATUS_RED_RIGHT, false);
		gpio_set_pin_level(STATUS_ORANGE_RIGHT, false);
		dashData.shiftLightFlashCounter = 0;
		return;
	}
	
	if (dashData.rpm > SHIFT_INDICATOR_RPM_OFF)
	{
		gpio_set_pin_level(STATUS_ORANGE_LEFT, true);
		gpio_set_pin_level(STATUS_RED_LEFT, false);
		gpio_set_pin_level(STATUS_RED_RIGHT, false);
		gpio_set_pin_level(STATUS_ORANGE_RIGHT, false);
		dashData.shiftLightFlashCounter = 0;
		return;
	}
}