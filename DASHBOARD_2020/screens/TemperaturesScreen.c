/*
 * TemperaturesScreen.c
 *
 * Created: 15.02.2020 14:14:40
 *  Author: Igor
 */ 
#include "screens.h"

static void draw()
{
	updateTextTemperature(dashData.waterTemp1);
	oledl128_string(18, 0, font_16x32nums, textData.temperature, false);
	oledl128_string(8, 4, font_8x16, labelWater, true);
	
	updateTextTemperature(dashData.oilTemp);
	oledl128_string(90, 0, font_16x32nums, textData.temperature, false);
	oledl128_string(80, 4, font_8x16, labelOil, true);
}

static void init()
{
	// TODO
}

Screen TemperaturesScreen =
{
	.initialized = false,
	.showStatusbar = true,
	.drawScreen = draw,
	.initScreen = init
};