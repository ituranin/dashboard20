/*
 * ModeSelectionScreen.c
 *
 * Created: 11.02.2020 18:44:55
 *  Author: Igor
 */ 
#include "../screens.h"

static char *question = "> MANUAL MODE?";
static char *modeAcceptButtonLabel = " press IGN ";

void manualModeAcceptHandler(int level);

static void draw()
{
	oledl128_string(8, 2, font_8x16, question, false);
	oledl128_string(52, 4, font_6x8, modeAcceptButtonLabel, true);
}

static void init()
{
	ModeSelectionScreen.btnMidRightHandler = manualModeAcceptHandler;
}

void manualModeAcceptHandler(int level)
{
	if(!level)
	{
		return;
	}
	
	setMode(CV);
}

Screen ModeSelectionScreen =
{
	.initialized = false,
	.showStatusbar = true,
	.drawScreen = draw,
	.initScreen = init
};