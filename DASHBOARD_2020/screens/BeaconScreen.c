/*
 * BeaconScreen.c
 *
 * Created: 15.02.2020 12:23:52
 *  Author: Igor
 */ 
#include "screens.h"

static struct timer_task beaconTimer;

static bool runBeacon = false;
static uint8_t minutes = 0;
static uint8_t seconds = 0;
static uint16_t milliSeconds = 0;
static uint32_t milliSecondsThis = 0;
static uint32_t milliSecondsFastest = 0;
static char fastestTimeText[] = " 0.00:000 ";

static void beaconTask(const struct timer_task *const timer_task)
{
	if (!runBeacon)
	{
		return;
	}
	
	milliSecondsThis++;
	milliSeconds++;
	
	if (milliSeconds >= 1000)
	{
		milliSeconds = 0;
		seconds++;
		
		if (seconds >= 60)
		{
			seconds = 0;
			minutes++;
		}
	}
}

void resetTextTimeFastest()
{
	fastestTimeText[1] = '0';
	fastestTimeText[3] = '0';
	fastestTimeText[4] = '0';
	fastestTimeText[6] = '0';
	fastestTimeText[7] = '0';
	fastestTimeText[8] = '0';
}

void copyTextTimeToFastest(const char* beaconText)
{
	fastestTimeText[1] = beaconText[0];
	fastestTimeText[3] = beaconText[2];
	fastestTimeText[4] = beaconText[3];
	fastestTimeText[6] = beaconText[5];
	fastestTimeText[7] = beaconText[6];
	fastestTimeText[8] = beaconText[7];
}

void resetBeacon(int level)
{
	if(!level)
	{
		return;
	}
	
	runBeacon = false;
	milliSecondsThis = 0;
	milliSecondsFastest = 0;
	minutes = 0;
	seconds = 0;
	milliSeconds = 0;
	resetTextTimeFastest();
}

void nextRound(int level)
{
	if(!level)
	{
		return;
	}
	
	runBeacon = false;
	
	if (milliSecondsFastest == 0 || milliSecondsFastest > milliSecondsThis)
	{
		milliSecondsFastest = milliSecondsThis;
		copyTextTimeToFastest(textData.beacon);
	}
	
	milliSecondsThis = 0;
	minutes = 0;
	seconds = 0;
	milliSeconds = 0;
	
	runBeacon = true;
}

static void draw()
{
	updateTextBeacon(minutes, seconds, milliSeconds);
	oledl128_string(0, 0, font_16x32nums, textData.beacon, false);
	oledl128_string(46, 4, font_8x16, fastestTimeText, true);
}

static void init()
{
	beaconTimer.interval = 1;
	beaconTimer.cb       = beaconTask;
	beaconTimer.mode     = TIMER_TASK_REPEAT;
	timer_add_task(&TIMER_0, &beaconTimer);
	
	BeaconScreen.btnShiftUpHandler = nextRound;
	BeaconScreen.btnShiftDownHandler = resetBeacon;
}

Screen BeaconScreen =
{
	.initialized = false,
	.showStatusbar = true,
	.drawScreen = draw,
	.initScreen = init
};