/*
 * BrakeTestScreen.c
 *
 * Created: 12.02.2020 16:01:35
 *  Author: Igor
 */ 
#include "../screens.h"

static char *tireDelimiter = " | ";
static char *tireCharacter = "*";

static bool blockedFL = false;
static bool blockedFR = false;
static bool blockedRL = false;
static bool blockedRR = false;

static void checkBlockingState()
{
	if (dashData.brakePressureFront == 0 && dashData.brakePressureRear == 0)
	{
		blockedFL = false;
		blockedFR = false;
		blockedRL = false;
		blockedRR = false;
		return;
	}
	
	blockedFL = dashData.wheelSpeedFL == 0 ? true : false;
	blockedFR = dashData.wheelSpeedFR == 0 ? true : false;
	blockedRL = dashData.wheelSpeedRL == 0 ? true : false;
	blockedRR = dashData.wheelSpeedRR == 0 ? true : false;
}

static void draw()
{
	checkBlockingState();
	updateTextSpeed(dashData.speed);
	oledl128_string(20, 1, font_8x16, tireDelimiter, false);
	oledl128_string(12, 1, font_8x16, tireCharacter, blockedFL);
	oledl128_string(44, 1, font_8x16, tireCharacter, blockedFR);
	oledl128_string(20, 3, font_8x8, tireDelimiter, false);
	oledl128_string(20, 4, font_8x16, tireDelimiter, false);
	oledl128_string(12, 4, font_8x16, tireCharacter, blockedRL);
	oledl128_string(44, 4, font_8x16, tireCharacter, blockedRR);
	oledl128_string(90, 1, font_16x32nums, textData.speed, false);
	oledl128_string(84, 5, font_6x8, labelKmh, true);
}

static void init()
{
	// TODO
}

Screen BrakeTestScreen =
{
	.initialized = false,
	.showStatusbar = false,
	.drawScreen = draw,
	.initScreen = init
};