/*
 * DvSensorsScreen.c
 *
 * Created: 15.02.2020 13:55:42
 *  Author: Igor
 */ 
#include "../screens.h"

static void draw()
{
	oledl128_string(16, 1, font_8x16, labelCPU, dashData.stateCPU);
	oledl128_string(76, 1, font_8x16, labelCamera, dashData.stateCamera);
	oledl128_string(16, 4, font_8x16, labelIMU, dashData.stateIMU);
	oledl128_string(76, 4, font_8x16, labelLidar, dashData.stateLidar);
}

static void init()
{
	// TODO
}

Screen DvSensorsScreen =
{
	.initialized = false,
	.showStatusbar = true,
	.drawScreen = draw,
	.initScreen = init
};