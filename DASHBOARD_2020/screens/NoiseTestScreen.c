/*
 * NoiseTestScreen.c
 *
 * Created: 11.02.2020 13:50:27
 *  Author: Igor
 */ 
#include "../screens.h"

static void draw()
{
	updateTextRpm(dashData.rpm, '0');
	oledl128_string(24, 1, font_16x32nums, textData.rpm, false);
	oledl128_string(62, 5, font_8x16, labelRpm, true);
}

static void init()
{
	// TODO
}

Screen NoiseTestScreen =
{
	.initialized = false,
	.showStatusbar = false,
	.drawScreen = draw,
	.initScreen = init
};