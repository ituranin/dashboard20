/*
 * BrakeBalanceScreen.c
 *
 * Created: 12.02.2020 14:29:56
 *  Author: Igor
 */ 
#include "../screens.h"

static void draw()
{
	updateTextBrakeBalance(dashData.brakePressureFront, dashData.brakePressureRear);
	oledl128_string(24, 1, font_16x32nums, textData.brakeBalance, false);
	oledl128_string(48, 5, font_6x8, labelBrakeBalance, true);
}

static void init()
{
	// TODO
}

Screen BrakeBalanceScreen =
{
	.initialized = false,
	.showStatusbar = false,
	.drawScreen = draw,
	.initScreen = init
};