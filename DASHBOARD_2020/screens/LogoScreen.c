/*
 * LogoScreen.c
 *
 * Created: 10.02.2020 19:02:12
 *  Author: Igor
 */ 

#include "screens.h"
#include "../oled_l128/images/logo_oled_bmp.h"

static void draw()
{
	oledl128_picture(0, 0, Image_logo_oled_bmp);
}

static void init()
{
	// TODO
}

Screen LogoScreen =
{
	.initialized = false,
	.showStatusbar = false,
	.drawScreen = draw,
	.initScreen = init
};