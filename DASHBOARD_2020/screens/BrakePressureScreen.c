/*
 * BrakePressureScreen.c
 *
 * Created: 14.02.2020 13:16:40
 *  Author: Igor
 */ 
#include "../screens.h"

static void draw()
{
	updateTextBrakePressure(dashData.brakePressureFront);
	oledl128_string(24, 2, font_8x16, textData.brakePressure, false);
	updateTextSpeed(30);
	
	updateTextBrakePressure(dashData.brakePressureRear);
	oledl128_string(72, 2, font_8x16, textData.brakePressure, false);
	
	oledl128_string(25, 4, font_6x8, labelBar, true);
	oledl128_string(73, 4, font_6x8, labelBar, true);
}

static void init()
{
	// TODO
}

Screen BrakePressureScreen =
{
	.initialized = false,
	.showStatusbar = false,
	.drawScreen = draw,
	.initScreen = init
};