/*
 * screenText.h
 *
 * Created: 13.02.2020 19:18:05
 *  Author: Igor
 */ 


#ifndef SCREENTEXT_H_
#define SCREENTEXT_H_

#include <atmel_start.h>

#define TEXT_BRAKE_BALANCE_STANDARD_LENGTH                              5
#define TEXT_BRAKE_PRESSURE_STANDARD_LENGTH                             4
#define TEXT_SPEED_STANDARD_LENGTH                                      2
#define TEXT_RPM_STANDARD_LENGTH                                        5
#define MAX_VOLTAGE                                                 16000
#define MIN_VOLTAGE                                                 11000
#define WARN_VOLTAGE                                                11500
#define VOLTAGE_FOR_PERCENTAGE_CHECK          (MAX_VOLTAGE - MIN_VOLTAGE)

static char *labelBrakeBalance = " Balance ";
static char *labelKmh = " km/h ";
static char *labelRpm = " RPM ";
static char *labelBar = " bar ";
static char *labelCV = "CV";
static char *labelDV = "DV";
static char *labelHydraulicsPump = "HP";
static char *statusBarDelimiter = "|";
static char *labelCPU = " CPU ";
static char *labelCamera = " CAM ";
static char *labelLidar = " LID ";
static char *labelIMU = " IMU ";
static char *labelCelsius = "C";
static char *labelWater = " WAT ";
static char *labelOil = " OIL ";

typedef struct  
{
	char *brakeBalance;
	char *speed;
	char *rpm;
	char *shutDownCircuitStatus;
	char *hydraulicsPumpStatus;
	char *brakePressure;
	char *batteryPercentage;
	char *beacon;
	char *temperature;
} TextData;

extern TextData textData;

void updateTextBrakeBalance(uint16_t front, uint16_t rear);
void updateTextBrakePressure(uint16_t pressure);
void updateTextSpeed(uint16_t speed);
void updateTextRpm(uint16_t rpm, char spacer);
void updateTextShutdownCircuitStatus(uint8_t status);
void updateTextHydraulicsPumpStatus(uint8_t status);
void updateTextBatteryPercentage(uint16_t voltage);
void updateTextBeacon(uint8_t minutes, uint8_t seconds, uint16_t milliSeconds);
void updateTextTemperature(uint16_t temperature);

#endif /* SCREENTEXT_H_ */