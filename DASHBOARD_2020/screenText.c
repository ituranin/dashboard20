/*
 * screenText.c
 *
 * Created: 13.02.2020 19:21:01
 *  Author: Igor
 */ 

#include "screenText.h"

// standard strings
static char *textBrakeBalanceNoPressure = "00:00";
static char *textBrakeBalanceFrontFullPressure = "100:0";
static char *textBrakeBalanceRearFullPressure = "0:100";

static char *textBrakePressureNoPressure = "00.0";
static char *textBrakePressureMax = "99.9";

static char *textMaxTwoDigitNumber = "99";
static char *textMinTwoDigitNumber = "00";

static char *textHyraulicsPumpFailure = "!";
static char *textHyraulicsPumpOk = "+";

static char *textBeaconZero = "0.00:000";

static char *textScClosed = "---";
static char *textScButtonDash = "SBD";
static char *textScButtonLeft = "SBL";
static char *textScButtonRight = "SBR";
static char *textScRes = "RES";
static char *textScCrash = "CRS";
static char *textScBspd = "BSP";
static char *textScEtc = "ETC";
static char *textScAssc = "ASC";
static char *textScBots = "BOT";

// declaring fixed length arrays like array[5] will cause undefined behavior with the functions
// possibly because of missing '\0'
// holder for text to show, have to be assigned to pointers in textData
static char textBrakeBalance[] = "__:__";
static char textBrakePressure[] = "__._";
static char textSpeed[] = "__";
static char textRpm[] = "_____";
static char textBatteryPercentage[] = "__";
static char textBeacon[] = "0.00:000";
static char textTemperature[] = "__";

// temporary array for utoa
static char tempIntToText[] = "00000";

TextData textData = 
{
	.brakeBalance = textBrakeBalance,
	.speed = textSpeed,
	.rpm = textRpm,
	.shutDownCircuitStatus = "",
	.hydraulicsPumpStatus = "",
	.brakePressure = textBrakePressure,
	.batteryPercentage = textBatteryPercentage,
	.beacon = textBeacon,
	.temperature = textTemperature
};

void updateTextBrakeBalance(uint16_t front, uint16_t rear)
{
	if (front == 0 && rear == 0)
	{
		textData.brakeBalance = textBrakeBalanceNoPressure;
		return;
	}
	
	if (front == 0)
	{
		textData.brakeBalance = textBrakeBalanceRearFullPressure;
		return;
	}
	
	if (rear == 0)
	{
		textData.brakeBalance = textBrakeBalanceFrontFullPressure;
		return;
	}
	
	uint32_t total = front + rear;
	uint32_t frontPercentage = (uint32_t)(front * 100) / total;
	uint32_t rearPercentage = 100 - frontPercentage;
	
	if (frontPercentage == 0)
	{
		textData.brakeBalance = textBrakeBalanceRearFullPressure;
		return;
	}
	
	if (rearPercentage == 0)
	{
		textData.brakeBalance = textBrakeBalanceFrontFullPressure;
		return;
	}
	
	textData.brakeBalance = textBrakeBalance;
	utoa(frontPercentage, tempIntToText, 10);
	
	if (frontPercentage < 10)
	{
		textBrakeBalance[0] = '0';
		textBrakeBalance[1] = tempIntToText[0];
	}
	else
	{
		textBrakeBalance[0] = tempIntToText[0];
		textBrakeBalance[1] = tempIntToText[1];
	}
	
	utoa(rearPercentage, tempIntToText, 10);
	
	if (rearPercentage < 10)
	{
		textBrakeBalance[3] = '0';
		textBrakeBalance[4] = tempIntToText[0];
	}
	else
	{
		textBrakeBalance[3] = tempIntToText[0];
		textBrakeBalance[4] = tempIntToText[1];
	}
}

void updateTextBrakePressure(uint16_t pressure)
{
	if (pressure == 0)
	{
		textData.brakePressure = textBrakePressureNoPressure;
		return;
	}
	
	if (pressure >= 10000)
	{
		textData.brakePressure = textBrakePressureMax;
		return;
	}
	
	textData.brakePressure = textBrakePressure;
	utoa(pressure, tempIntToText, 10);
	
	if (pressure < 100)
	{
		textBrakePressure[0] = '0';
		textBrakePressure[1] = '0';
		textBrakePressure[3] = tempIntToText[0];
	}
	else if (pressure < 1000)
	{
		textBrakePressure[0] = '0';
		textBrakePressure[1] = tempIntToText[0];
		textBrakePressure[3] = tempIntToText[1];
	}
	else
	{
		textBrakePressure[0] = tempIntToText[0];
		textBrakePressure[1] = tempIntToText[1];
		textBrakePressure[3] = tempIntToText[2];
	}
}

void updateTextSpeed(uint16_t speed)
{
	if (speed >= 1000)
	{
		textData.speed = textMaxTwoDigitNumber;
		return;
	}
	
	utoa(speed, tempIntToText, 10);
	
	textData.speed = textSpeed;
	
	if(speed < 100)
	{
		textSpeed[0] = '0';
		textSpeed[1] = tempIntToText[0];
		return;
	}
	
	textSpeed[0] = tempIntToText[0];
	textSpeed[1] = tempIntToText[1];
}

void updateTextRpm(uint16_t rpm, char spacer)
{
	uint8_t offset = 0;
	
	if (rpm >= 10000)
	{
		//offset = 0;
	}
	else if(rpm >= 1000)
	{
		offset = 1;
	}
	else if(rpm >= 100)
	{
		offset = 2;
	}
	else if(rpm >= 10)
	{
		offset = 3;
	}
	else
	{
		offset = 4;
	}
	
	utoa(rpm, tempIntToText, 10);
	
	for (int i = 0; i < TEXT_RPM_STANDARD_LENGTH; i++)
	{
		if (i < offset)
		{
			textRpm[i] = spacer;
		}
		else
		{
			textRpm[i] = tempIntToText[i - offset];
		}
	}
}

void updateTextShutdownCircuitStatus(uint8_t status)
{
	switch (status)
	{
		case 1: textData.shutDownCircuitStatus = textScButtonRight; break;
		case 2: textData.shutDownCircuitStatus = textScButtonLeft; break;
		case 3: textData.shutDownCircuitStatus = textScButtonDash; break;
		case 4: textData.shutDownCircuitStatus = textScCrash; break;
		case 5: textData.shutDownCircuitStatus = textScBots; break;
		case 6: textData.shutDownCircuitStatus = textScBspd; break;
		case 7: textData.shutDownCircuitStatus = textScEtc; break;
		case 8: textData.shutDownCircuitStatus = textScRes; break;
		case 9: textData.shutDownCircuitStatus = textScAssc; break;
		default: textData.shutDownCircuitStatus = textScClosed;
	}
}

void updateTextHydraulicsPumpStatus(uint8_t status)
{
	if (status)
	{
		textData.hydraulicsPumpStatus = textHyraulicsPumpOk;
		return;
	}
	
	textData.hydraulicsPumpStatus = textHyraulicsPumpFailure;
}

void updateTextBatteryPercentage(uint16_t voltage)
{
	if (voltage < MIN_VOLTAGE)
	{
		textData.batteryPercentage = textMinTwoDigitNumber;
		return;
	}
	
	if (voltage >= MAX_VOLTAGE)
	{
		textData.batteryPercentage = textMaxTwoDigitNumber;
		return;
	}
	
	uint32_t percentage = (((uint32_t) voltage - MIN_VOLTAGE) * 100) / VOLTAGE_FOR_PERCENTAGE_CHECK;
	
	utoa(percentage, tempIntToText, 10);
	
	textData.batteryPercentage = textBatteryPercentage;
	
	if(percentage < 10)
	{
		textBatteryPercentage[0] = '0';
		textBatteryPercentage[1] = tempIntToText[0];
		return;
	}
	
	textBatteryPercentage[0] = tempIntToText[0];
	textBatteryPercentage[1] = tempIntToText[1];
}

void updateTextBeacon(uint8_t minutes, uint8_t seconds, uint16_t milliSeconds)
{
	if (minutes > 9)
	{
		return;
	}
	
	if (minutes == 0 && seconds == 0 && milliSeconds == 0)
	{
		textData.beacon = textBeaconZero;
		return;
	}
	
	textData.beacon = textBeacon;
	
	utoa(minutes, tempIntToText, 10);
	
	textBeacon[0] = tempIntToText[0];
	
	utoa(seconds, tempIntToText, 10);
	
	if (seconds < 10)
	{
		textBeacon[2] = '0';
		textBeacon[3] = tempIntToText[0];
	}
	else
	{
		textBeacon[2] = tempIntToText[0];
		textBeacon[3] = tempIntToText[1];
	}
	
	utoa(milliSeconds, tempIntToText, 10);
	
	if (milliSeconds < 10)
	{
		textBeacon[5] = '0';
		textBeacon[6] = '0';
		textBeacon[7] = tempIntToText[0];
	}
	else if (milliSeconds < 100)
	{
		textBeacon[5] = '0';
		textBeacon[6] = tempIntToText[0];
		textBeacon[7] = tempIntToText[1];
	}
	else
	{
		textBeacon[5] = tempIntToText[0];
		textBeacon[6] = tempIntToText[1];
		textBeacon[7] = tempIntToText[2];
	}
}

void updateTextTemperature(uint16_t temperature)
{
	if (temperature >= 1000)
	{
		textData.temperature = textMaxTwoDigitNumber;
		return;
	}
	
	utoa(temperature, tempIntToText, 10);
	
	textData.temperature = textTemperature;
	
	if(temperature < 100)
	{
		textTemperature[0] = '0';
		textTemperature[1] = tempIntToText[0];
		return;
	}
	
	textTemperature[0] = tempIntToText[0];
	textTemperature[1] = tempIntToText[1];
}