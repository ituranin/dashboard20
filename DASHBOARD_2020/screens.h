/*
 * screens.h
 *
 * Created: 10.02.2020 17:48:08
 *  Author: Igor
 */ 


#ifndef SCREENS_H_
#define SCREENS_H_

#define NUMBER_OF_SCREENS_CV 8
#define NUMBER_OF_SCREENS_DV 2

#include "global.h"
#include "screenText.h"
#include "oled_l128/oled_SSD1309.h"
#include "oled_l128/fonts/font_16x32nums.h"
#include "oled_l128/fonts/font_8x16.h"
#include "oled_l128/fonts/font_8x8.h"
#include "oled_l128/fonts/font_6x8.h"

typedef void (*ButtonHandler)(int level);

typedef enum
{
	CV = 0,
	DV
} ScreenMode;

typedef struct
{
	bool initialized;
	bool showStatusbar;
	FUNC_PTR drawScreen;
	FUNC_PTR initScreen;
	ButtonHandler btnTopLeftHandler;
	ButtonHandler btnTopRightHandler;
	ButtonHandler btnMidLeftHandler;
	ButtonHandler btnMidRightHandler;
	ButtonHandler btnBottomLeftHandler;
	ButtonHandler btnBottomRightHandler;
	ButtonHandler btnShiftUpHandler;
	ButtonHandler btnShiftDownHandler;
} Screen;

typedef struct
{
	Screen *screens;
	Screen *currentScreen;
	Screen *cvScreens[NUMBER_OF_SCREENS_CV];
	Screen *dvScreens[NUMBER_OF_SCREENS_DV];
	int mode;
	bool redraw;
	bool changeScreen;
	uint8_t numberOfScreens;
	int8_t currentScreenIndex;
} ScreenState;

/* Screen definitions */
extern Screen LogoScreen;
extern Screen NoiseTestScreen;
extern Screen ModeSelectionScreen;
extern Screen BrakeBalanceScreen;
extern Screen BrakePressureScreen;
extern Screen BrakeTestScreen;
extern Screen BeaconScreen;
extern Screen DvSensorsScreen;
extern Screen TemperaturesScreen;

/* Screen storage and switching variables*/
extern ScreenState screenState;

/* Screen storage and switching methods*/
void initScreens();
void setMode(ScreenMode mode);
void updateScreen();
void drawInfoBar();
void nextScreen();
void previousScreen();
void requestClearScreen();
void requestRedrawScreen();

/* Standard Button Handlers*/
// TODO make own file for this
void standardShiftUpHandler(int level);
void standardShiftDownHandler(int level);
void standardBtnTopLeftHandler(int level);
void standardBtnTopRightHandler(int level);
void standardBtnMidLeftHandler(int level);
void standardBtnMidRightHandler(int level);
void standardBtnBottomLeftHandler(int level);
void standardBtnBottomRightHandler(int level);

#endif /* SCREENS_H_ */