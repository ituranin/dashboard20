/*
 * global.h
 *
 * Created: 10.02.2020 13:28:26
 *  Author: Igor
 */ 


#ifndef GLOBAL_H_
#define GLOBAL_H_

#include <atmel_start.h>

#define SHIFT_INDICATOR_RPM_OFF                                8000
#define SHIFT_INDICATOR_RPM_1                                  8500
#define SHIFT_INDICATOR_RPM_2                                  9000
#define SHIFT_INDICATOR_RPM_3                                  9500
#define LED_FLASH_COUNTER_INFLECTION                              5
#define LED_FLASH_COUNTER_RESET                                  10
#define ACTOR_MIN_VOLTAGE_RAW                                 11000
#define SYSTEM_MIN_VOLTAGE_RAW                                11000
#define OIL_PRESSURE_MIN_RAW                                    150
#define MAX_DOWNSHIFT_SPEED_FIRST_GEAR_RAW                      139
#define UPSHIFT_REQUEST                                           2
#define DOWNSHIFT_REQUEST                                         1
#define CLEARSHIFT_REQUEST                                        0

enum BatType {
	liFePo = 0,
	liIon
};

typedef struct
{
	bool autoShiftEnabled;
	bool engineIsRunning;

	bool ebsFailure;
	
	uint8_t gear;
	volatile uint16_t rpm;
	uint16_t oilTemp;		//divide by 10 to get C
	uint16_t oilPressure;	//in mBar
	uint16_t waterTemp1;	//divide by 10 to get C
	uint16_t waterTemp2;	//divide by 10 to get C

	uint8_t batSysType;
	uint8_t batActType;
	uint16_t batVoltAct;		//in mV
	uint16_t batVoltSys;		//in mV
	uint16_t batTempAct;		//divide by 10 to get C
	uint16_t batTempSys;		//divide by 10 to get C

	uint16_t speed;		//in 10*km/h
	
	uint16_t wheelSpeedFL;
	uint16_t wheelSpeedFR;
	uint16_t wheelSpeedRL;
	uint16_t wheelSpeedRR;

	uint16_t laptime;		//in 0.01s
	uint16_t prevLaptime;	//in 0.01s
	uint16_t dLaptime;		//in 0.01s
	uint16_t fastestLaptime; //in 0.01s
	uint8_t beaconTriggered;  //timer to display laptimeDelta
	bool displayFastestLap;
	uint8_t blockFastestLaptime;

	uint16_t brakePressureFront;	//in 0.01 bar
	uint16_t brakePressureRear;	//in 0.01 bar

	uint8_t limpMode;			//codes see ECU

	bool ascuIsConnected;
	
	uint8_t shiftLightFlashCounter;
	uint8_t shiftRequestState;
	bool sendCommandMessage;
	
	uint8_t shutDownCircuitState;
	uint8_t hydraulicsPumpState;
	uint8_t state;
	uint8_t amiState;
	
	uint8_t stateCPU;
	uint8_t stateIMU;
	uint8_t stateCamera;
	uint8_t stateLidar;
} DashData;

extern DashData dashData;

void downshiftRequest();
void upshiftRequest();
void clearShiftRequest();
bool checkUpshiftConditions();
bool checkDownshiftConditions();

void setGearDisplay();
void setRpmLeds();
void setActorBatteryLed();
void setSystemBatteryLed();
void setEbsIndicatorLed();
void setOilPressureLow();

void setRpmLedsDebug();

#endif /* GLOBAL_H_ */