/*
 * screens.c
 *
 * Created: 11.02.2020 12:21:05
 *  Author: Igor
 */ 
#include "screens.h"

ScreenState screenState =
{
	.screens = 0,
	.currentScreen = &ModeSelectionScreen,
	.cvScreens = {&TemperaturesScreen, &DvSensorsScreen, &BeaconScreen, &BrakePressureScreen, &BrakeTestScreen, &BrakeBalanceScreen, &LogoScreen, &NoiseTestScreen},
	.dvScreens = {&LogoScreen, &NoiseTestScreen},
	.currentScreenIndex = 0,
	.numberOfScreens = 0,
	.changeScreen = false,
	.redraw = false,
	.mode = CV
};

void assignStandardButtonHandlers(Screen *screen)
{
	screen->btnTopLeftHandler = 0;
	screen->btnTopRightHandler = 0;
	screen->btnMidLeftHandler = standardBtnMidLeftHandler;
	screen->btnMidRightHandler = standardBtnMidRightHandler;
	screen->btnBottomLeftHandler = 0;
	screen->btnBottomRightHandler = 0;
	screen->btnShiftUpHandler = standardShiftUpHandler;
	screen->btnShiftDownHandler = standardShiftDownHandler;
}

void initScreens()
{
	// TODO function
	assignStandardButtonHandlers(&ModeSelectionScreen);
	ModeSelectionScreen.initScreen();
	ModeSelectionScreen.initialized = true;
	
	for (int i = 0; i < NUMBER_OF_SCREENS_CV; i++)
	{
		assignStandardButtonHandlers(screenState.cvScreens[i]);
	}
	
	for (int i = 0; i < NUMBER_OF_SCREENS_CV; i++)
	{
		screenState.cvScreens[i]->initScreen();
		screenState.cvScreens[i]->initialized = true;
	}
	
	for (int i = 0; i < NUMBER_OF_SCREENS_DV; i++)
	{
		if (screenState.dvScreens[i]->initialized)
		{
			continue;
		}
		
		assignStandardButtonHandlers(screenState.dvScreens[i]);
	}
	
	for (int i = 0; i < NUMBER_OF_SCREENS_DV; i++)
	{
		if (screenState.dvScreens[i]->initialized)
		{
			continue;
		}
		
		screenState.dvScreens[i]->initScreen();
		screenState.dvScreens[i]->initialized = true;
	}
	
	screenState.screens = *screenState.cvScreens;
	screenState.numberOfScreens = NUMBER_OF_SCREENS_CV;
	screenState.mode = CV;
}

void setMode(ScreenMode mode)
{
	screenState.currentScreenIndex = 0;
	
	switch(mode)
	{
		case DV: screenState.screens = *screenState.dvScreens; screenState.numberOfScreens = NUMBER_OF_SCREENS_DV; screenState.mode = DV; break;
		default: screenState.screens = *screenState.cvScreens; screenState.numberOfScreens = NUMBER_OF_SCREENS_CV; screenState.mode = CV;
	}
	
	screenState.currentScreen = &screenState.screens[screenState.currentScreenIndex];
	requestClearScreen();
	requestRedrawScreen();
}

void nextScreen()
{
	screenState.currentScreenIndex++;
	
	if (screenState.currentScreenIndex == screenState.numberOfScreens)
	{
		screenState.currentScreenIndex = 0;
	}
	
	screenState.currentScreen = screenState.cvScreens[screenState.currentScreenIndex];
	requestClearScreen();
}

void previousScreen()
{
	screenState.currentScreenIndex--;
	
	if (screenState.currentScreenIndex < 0)
	{
		screenState.currentScreenIndex = screenState.numberOfScreens - 1;
	}
	
	screenState.currentScreen = screenState.cvScreens[screenState.currentScreenIndex];
	requestClearScreen();
}

void requestClearScreen()
{
	screenState.changeScreen = true;
}

void requestRedrawScreen()
{
	screenState.redraw = true;
}

void updateScreen()
{	
	if (!screenState.redraw)
	{
		return;
	}
	
	screenState.redraw = false;
	
	if (screenState.changeScreen)
	{
		oledl128_clear();
		screenState.changeScreen = false;
	}
	
	screenState.currentScreen->drawScreen();
	
	drawInfoBar();
}

bool batteryEmptySoon(uint16_t voltage)
{
	if (voltage < WARN_VOLTAGE)
	{
		return true;
	}
	
	return false;
}

void drawInfoBar()
{
	if (!screenState.currentScreen->showStatusbar)
	{
		return;
	}
	
	oledl128_rectangle(0, 6, 127, 6, 0x80);
	
	if (screenState.mode == CV)
	{
		oledl128_string(0, 7, font_8x8, labelCV, true);
	}
	else
	{
		oledl128_string(0, 7, font_8x8, labelDV, true);
	}
	
	oledl128_string(16, 7, font_8x8, statusBarDelimiter, true);
	
	updateTextBatteryPercentage(dashData.batVoltAct);
	oledl128_string(24, 7, font_8x8, textData.batteryPercentage, !batteryEmptySoon(dashData.batVoltAct));
	
	oledl128_string(40, 7, font_8x8, statusBarDelimiter, true);
	
	updateTextBatteryPercentage(dashData.batVoltSys);
	oledl128_string(48, 7, font_8x8, textData.batteryPercentage, !batteryEmptySoon(dashData.batVoltSys));
	
	oledl128_string(64, 7, font_8x8, statusBarDelimiter, true);
	
	oledl128_string(72, 7, font_8x8, labelHydraulicsPump, true);
	updateTextHydraulicsPumpStatus(dashData.hydraulicsPumpState);
	oledl128_string(88, 7, font_8x8, textData.hydraulicsPumpStatus, true);
	
	oledl128_string(96, 7, font_8x8, statusBarDelimiter, true);
	
	updateTextShutdownCircuitStatus(dashData.shutDownCircuitState);
	oledl128_string(104, 7, font_8x8, textData.shutDownCircuitStatus, true);
}

/* Standard Button Handlers*/
void standardShiftUpHandler(int level)
{
	if(!level)
	{
		return;
	}
	
	upshiftRequest();
}

void standardShiftDownHandler(int level)
{
	if(!level)
	{
		return;
	}
	
	downshiftRequest();
}

void standardBtnTopLeftHandler(int level)
{
	
}

void standardBtnTopRightHandler(int level)
{
	
}

void standardBtnMidLeftHandler(int level)
{
	if(!level)
	{
		return;
	}
	
	previousScreen();
}

void standardBtnMidRightHandler(int level)
{
	if(!level)
	{
		return;
	}
	
	nextScreen();
}

void standardBtnBottomLeftHandler(int level)
{
	
}

void standardBtnBottomRightHandler(int level)
{
	
}
