# Dashboard20

Dies ist der Code für das Multi-Funktions-Lenkrad mit Display für den BRC20.

## Funktionen
* Up-Shift, Down-Shift über Pedale
* Kupplung über Pedal mit Linearpotenziometer
* OLED-Display mit einem Software-Screen-System für verschiedene Informationsgruppen bzw. Seiten
* CAN-Anbindung
* Ansteuerung der Warn-LEDs für Temperatur - Wasser / Öl
* Ansteuerung der RPM-LED-Anzeige
* Ansteuerung des Schalt-Blitzes

## Foto
Hier ist ein Beispiel des Interfaces mit Braketest-Screen:
![Lenkrad-Platine](pcb_dashboard20.jpg)
